import bpy
import os

renders_folder = bpy.path.abspath("//renders")
rendered_scenes = sorted(os.listdir(renders_folder))
current_frame = 0
bpy.context.window.scene = bpy.data.scenes["00_EDIT"]

for scene in rendered_scenes:
    if scene == '00_EDIT':
        continue

    folder = renders_folder + '/' + scene
    first, *rest = sorted(os.listdir(folder))
    image_path = folder + '/' + first

    imstrip = bpy.context.scene.sequence_editor.sequences.new_image(
        name=scene,
        filepath=image_path,
        channel=2,
        frame_start=current_frame,
        fit_method='ORIGINAL'
    )

    for image in rest:
        imstrip.elements.append(image)

    current_frame += len(os.listdir(folder))
    print('added ' + scene)
